<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Psy\Readline\Transient;

class TransactionController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'transaction_type' => 'required|in:deposit,withdrawal',
            'amount' => 'required|numeric|min:0',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $validated = $validator->validated();

        $user = Auth::user();
        $validated['user_id'] = $user->id;
        $amount = $request->input('amount');

        DB::beginTransaction();

        try {

            $withdrawalFee = 0;
            if ($validated['transaction_type'] === 'withdrawal') {
                if ($user->account_type === 'individual') {
                    $today = Carbon::now();
                    if ($today->dayOfWeek === Carbon::FRIDAY) {
                        $withdrawalFee = 0;
                    } elseif ($user->withdrawalsThisMonth() < 5 && $amount <= 1000) {
                        $withdrawalFee = 0;
                    } else {
                        $withdrawalFee = $amount * ($user->account_type === 'business' ? 0.025 : 0.015);
                    }
                } elseif ($user->account_type === 'business' && $user->totalWithdrawals() >= 50000) {
                    $withdrawalFee = $amount * 0.015;
                } else {
                    $withdrawalFee = $amount * ($user->account_type === 'business' ? 0.025 : 0.015);
                }
            }

            // Create a transaction
            $validated['fee'] = $withdrawalFee;
            $validated['date'] = now();
            Transaction::create($validated);

            // Update the user's balance based on transaction type
            if ($validated['transaction_type'] === 'deposit') {
                $user->increment('balance', $amount);
            } elseif ($validated['transaction_type'] === 'withdrawal') {
                $user->decrement('balance', ($amount + $withdrawalFee));
            }

            DB::commit();

            return redirect("login")->withSuccess('Transaction successful');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect("login")->withErrors('Transaction failed');
        }
    }
}
