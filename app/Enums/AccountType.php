<?php
namespace App\Enums;

use Spatie\Enum\Enum;

/**
 * @method static self individual()
 * @method static self business()
 */
class AccountType extends Enum
{
    protected static function values(): array
    {
        return [
            'individual' => 'Individual',
            'business' => 'Business',
        ];
    }

    public static function toSelectArray(): array
    {
        return [
            'individual' => __('Individual'),
            'business' => __('Business'),
        ];
    }
}

