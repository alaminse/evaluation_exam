<?php
namespace App\Enums;

use Spatie\Enum\Enum;

/**
 * @method static self withdrawal()
 * @method static self deposit()
 */
class TransactionType extends Enum
{
    protected static function values(): array
    {
        return [
            'withdrawal' => 'Withdrawal',
            'deposit' => 'Deposit',
        ];
    }

    public static function toSelectArray(): array
    {
        return [
            'withdrawal' => __('Withdrawal'),
            'deposit' => __('Deposit'),
        ];
    }
}

