<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name', 'account_type', 'balance', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
        'account_type' => 'string',
    ];

    public function withdrawals(): HasMany
    {
        return $this->hasMany(Transaction::class)->where('transaction_type', 'Withdrawal');
    }

    public function deposits(): HasMany
    {
        return $this->hasMany(Transaction::class)->where('transaction_type', 'Deposit');
    }

    public function withdrawalsThisMonth()
    {
        $currentMonth = Carbon::now()->format('Y-m');

        return $this->transactions()
            ->where('transaction_type', 'Withdrawal')
            ->whereYear('date', Carbon::now()->year)
            ->whereMonth('date', Carbon::now()->month)
            ->count();
    }
    
    public function totalWithdrawals()
    {
        return $this->transactions()
            ->where('transaction_type', 'Withdrawal')
            ->sum('amount');
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
}
