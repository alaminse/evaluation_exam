<?php

use App\Http\Controllers\TransactionController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::post('/create/user', 'UserController@create')->name('users.create');
// Route::post('/users', 'UserController@store')->name('users.store');
// Route::post('/login', 'UserController@login');
// Route::get('/', 'TransactionController@index');
// Route::get('/deposit', 'TransactionController@deposit');
// Route::post('/deposit', 'TransactionController@storeDeposit');
// Route::get('/withdrawal', 'TransactionController@withdrawal');
// Route::post('/withdrawal', 'TransactionController@storeWithdrawal');

Route::controller(UserController::class)
    ->prefix('users')
    ->as('users.')
    ->group(function () {
        Route::get('/create', 'create')->name('create');
        Route::post('/store', 'store')->name('store');
    });

Route::controller(TransactionController::class)
    ->prefix('transactions')
    ->as('transactions.')
    ->group(function () {
        Route::post('/store', 'store')->name('store');
        // Route::get('/create', 'create')->name('create');
        // Route::post('/store', 'store')->name('store');
        // Route::get('/show/{transaction}', 'show')->name('show');
        // Route::get('/edit/{transaction}', 'edit')->name('edit');
        // Route::post('/update/{transaction}', 'update')->name('update');
        // Route::get('/destroy/{transaction}', 'destroy')->name('destroy');
        // Route::get('/status/{transaction}', 'status')->name('status');
    });

Route::get('login', [UserController::class, 'loginForm'])->name('login')->middleware('guest');
Route::post('login', [UserController::class, 'login'])->name('login')->middleware('guest');
Route::get('dashboard', [UserController::class, 'dashboard'])->name('dashboard')->middleware('auth');
Route::get('signout', [UserController::class, 'signOut'])->name('signout');
