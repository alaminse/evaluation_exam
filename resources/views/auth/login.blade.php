@extends('app')
@section('contant')
    <div class="alert alert-success mt-5" role="alert">
        <h3>Login</h3>
    </div>
    <form method="POST" action="{{ route('login') }}">
        @csrf

        @include('inc.message')

        <div class="mb-3">
            <label for="email" class="form-label">Email address</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com" required>
        </div>

        <div class="mb-3">
            <label for="password" class="form-label">Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Enter your password" required>
        </div>

        <button type="submit" class="btn btn-primary">Login</button>
    </form>
@endsection
