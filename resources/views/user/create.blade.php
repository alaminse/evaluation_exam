@extends('app')
@section('contant')
    <div class="alert alert-success mt-5" role="alert">
        <h3>Create User</h3>
    </div>
    <form method="POST" action="{{ route('users.store') }}">
        @csrf

        @include('inc.message')

        <div class="mb-3">
            <label for="name" class="form-label">Name</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Enter your name" required>
        </div>

        <div class="mb-3">
            <label for="account_type" class="form-label">Account Type</label>
            <select class="form-select" id="account_type" name="account_type" required>
                @foreach(\App\Enums\TransactionType::toSelectArray() as $value => $label)
                    <option value="{{ $value }}">{{ $label }}</option>
                @endforeach
            </select>
        </div>

        <div class="mb-3">
            <label for="balance" class="form-label">Balance</label>
            <input type="number" class="form-control" id="balance" name="balance" placeholder="Enter balance" required>
        </div>

        <div class="mb-3">
            <label for="email" class="form-label">Email address</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com" required>
        </div>

        <div class="mb-3">
            <label for="password" class="form-label">Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Enter your password" required>
        </div>

        <button type="submit" class="btn btn-primary mb-5">Create</button>
    </form>
@endsection
