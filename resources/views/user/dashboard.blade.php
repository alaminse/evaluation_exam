@extends('app')
@section('contant')
    <div class="alert alert-success mt-5" role="alert">
        <h3>Dashboard</h3>
        <h6>
            Name: {{ Auth::user()->name }}
        </h6>
        Balance: {{ $data['balance'] }}
    </div>
    @include('inc.message')

    <div class="row">
        <h4 class="text-center text-bg-secondary p-3">Banking Transaction Form</h4>
        <form method="POST" action="{{ route('transactions.store') }}">
            @csrf
            <div class="mb-3">
                <label for="amount" class="form-label">Amount</label>
                <input type="number" class="form-control" id="amount" name="amount" placeholder="Enter amount" required>
            </div>

            <div class="mb-3">
                <label for="transaction_type" class="form-label">Transaction Type</label>
                <select class="form-select" id="transaction_type" name="transaction_type" required>
                    @foreach (\App\Enums\TransactionType::toSelectArray() as $value => $label)
                        <option value="{{ $value }}">{{ $label }}</option>
                    @endforeach
                </select>
            </div>

            <button type="submit" class="btn btn-success mb-5">Transaction</button>
        </form>
    </div>

    <div class="row">
        <h4 class="text-center text-bg-secondary p-3">Withdrawals</h4>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Fee</th>
                    <th scope="col">Date</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data['withdrawals'] as $key => $item)
                    <tr>
                        <th scope="row">{{ ++$key }}</th>
                        <td>{{ $item->amount }}</td>
                        <td>{{ $item->fee }}</td>
                        <td>{{ $item->date }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="row">
        <h4 class="text-center text-bg-secondary p-3">Deposits</h4>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Fee</th>
                    <th scope="col">Date</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data['withdrawals'] as $key => $item)
                    <tr>
                        <th scope="row">{{ ++$key }}</th>
                        <td>{{ $item->amount }}</td>
                        <td>{{ $item->fee }}</td>
                        <td>{{ $item->date }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
